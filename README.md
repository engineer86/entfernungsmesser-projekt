# Raspberry Pi Distance Measurement System

## Overview
![Raspberry Pi Distance Measurement](/presentation/pics/complete_side_pic.JPG)

This project features a Raspberry Pi 3B+ paired with a self-manufactured HAT board, which is outfitted with an I2C display, a buzzer, a button, and two ultrasonic sensors, to construct a sophisticated distance measurement system. Operating in two distinct modes—distance measurement and midpoint calculation—it emulates the functionality of contemporary vehicle PDC (Park Distance Control) systems, offering both audio and visual feedback via the display and buzzer for an interactive user experience.

## Hardware Requirements

- Raspberry Pi 3B+
- Custom HAT Board
- I2C Display
- Piezo Buzzer
- Button
- 2x HC-SR04 Ultrasonic Sensors

## Installation

### Directory Structure

- `bin` -> Compiled binary
- `lib` -> Libraries
- `src` -> Source code and Makefile

### Compilation

Navigate to the `src` directory and use the following Make commands:

- **Compile:** `make`
- **Clean:** `make clean`
- **Compile and Run:** `make run`

## Usage

The system starts by measuring distances or calculating the midpoint between two objects, showcasing the versatility of Raspberry Pi in handling real-world sensor data. It provides an exemplary case of integrating hardware components and software to solve practical problems.

## Demonstrations and Documentation

- **Demonstration Video:** [Watch on YouTube](https://www.youtube.com/watch?v=JNT4-5mAj-c)
- **In-depth Documentation:** [View on GitLab](https://gitlab.com/engineer86/entfernungsmesser-projekt/-/blob/master/Wiki.pdf)

## Contributing

Contributions to this project are welcome. Please ensure your pull requests are well-documented.

## License

This project is licensed under the MIT License - see the LICENSE.md file for details.
