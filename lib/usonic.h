/*
*************************************************
Autoren: Philipp Horleander und Konrad Münch 
Projekt: Entferungsmesser für CSE 2020
Datum: April bis Juli 2020
*************************************************
Datei: usonic.h
Bauteilbibliothek : Ultraschallsensorik

 */

#ifndef USONIC_H
#define USONIC_H

#include "gpio.h"
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include <inttypes.h>



int init_ref_time();

// Funktions für Microsekunden
uint64_t microsec();

// Messfunktion für Ultraschallsensor
float measure(int p_trig, int p_echo, int timeout);

#endif