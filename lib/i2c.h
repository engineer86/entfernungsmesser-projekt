/*
*************************************************
Autoren: Philipp Horleander und Konrad Münch 
Projekt: Entferungsmesser für CSE 2020
Datum: April bis Juli 2020
*************************************************
Datei: i2c.h
I2C-Bibliothek
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>


// I2C Definition
#define I2C_SLAVE	0x0703
#define I2C_SMBUS	0x0720

#define I2C_SMBUS_READ	1
#define I2C_SMBUS_WRITE	0

// SMBus Übertragungs Typen
#define I2C_SMBUS_QUICK		    0
#define I2C_SMBUS_BYTE		    1
#define I2C_SMBUS_BYTE_DATA	    2 
#define I2C_SMBUS_WORD_DATA	    3
#define I2C_SMBUS_PROC_CALL	    4
#define I2C_SMBUS_BLOCK_DATA	    5
#define I2C_SMBUS_I2C_BLOCK_BROKEN  6
#define I2C_SMBUS_BLOCK_PROC_CALL   7		
#define I2C_SMBUS_I2C_BLOCK_DATA    8

// SMBus Nachrichten
#define I2C_SMBUS_BLOCK_MAX	32

union i2c_smbus_data
{
	uint8_t	byte;
	uint16_t word;
	uint8_t block [I2C_SMBUS_BLOCK_MAX + 2];
};

struct i2c_smbus_ioctl_data
{
	char read_write;
	uint8_t command;
	int size;
	union i2c_smbus_data *data;
};

// Zugriff auf den SM-Bus
int i2c_smbus_access (int fd, char rw, uint8_t command, int size, union i2c_smbus_data *data);


//Setup des I2C Slaves
int i2c_setup(int i2c_slave);

// Schreiben in den Bus
int i2c_write(int fd, int data);

// Schreiben in das Register
int i2c_write_reg_8 (int fd, int reg, int data);

//Lesen des SM-Bus Registers
int i2c_read_reg_8 (int fd, int reg);