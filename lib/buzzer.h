/*
*************************************************
Autoren: Philipp Horleander und Konrad Münch 
Projekt: Entferungsmesser für CSE 2020
Datum: April bis Juli 2020
*************************************************
Datei: buzzer.h
Bauteilbibliothek für Buzzer

 */

#ifndef BUZZER_H
#define BUZZER_H

#include "gpio.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DELAY_DURATION_CHANGE_MODE 100000
#define DELAY_DURATION_DISTANCE 100000
#define DELAY_DURATION_START_SOUND 10000

#define BUZZER_PIN 4 // GPIO PIN 4


//Dauerhaftes Piepen des Buzzers
int beep_all_the_time_start();

//Ton aus an Buzzer
int beep_all_the_time_stop();

// Tonsignal wenn Modus gewechselt wird
void beep_change_mode();

// Gibt Ton für Starten aus
void beep_start_device();

// Funktion um Piepton auszugeben wenn Mittelpunkt gefunden wurde
void beep_middle(float left_sens,float right_sens);


#endif