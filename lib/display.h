/*
*************************************************
Autoren: Philipp Horleander und Konrad Münch 
Projekt: Entferungsmesser für CSE 2020
Datum: April bis Juli 2020
*************************************************
Datei: display.h
Bauteilbibliothek : OLED-Display
********************************************************************
Anmerkung zur Fremdquelle: 
Insperiert durch ssd1306_i2c - Bibliothek von Xiang, Zuo für WiringPi:
SSD1306 OLED Driver Programm(using I2C Interface)
Depend on lib wiringPi
Author : Xiang,Zuo
Mail   : xianglinks@gmail.com
********************************************************************
 */

#include "i2c.h"
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


//Defines für DISPLAY- Funktionen
#define SSD1306_DISPLAY_ON        0xAF
#define SSD1306_DISPLAY_OFF       0xAE
#define SSD1306_NORMAL_DISPLAY    0xA6
#define SSD1306_INVERT_DISPLAY    0xA7
#define SSD1306_MULTI_RATIO       0xA8
#define SSD1306_SET_DISPLAY_OFFSET 0xD3
#define SSD1306_SET_DISPLAY_START  0x40
#define SSD1306_SET_SEGMENT_REMAP  0xA1
#define SSD1306_SET_COM_OUT_DIR    0xC8
#define SSD1306_SET_COM_PINS       0xDA
#define SSD1306_SET_CONTRAST       0x81
#define SSD1306_SET_VCOMH_DES_LVL  0xDB
#define SSD1306_SET_DISPLAY_CLOCK  0xD5       
#define SSD1306_SET_MEM_ADR_MODE   0x20        
#define SSD1306_SET_COL_ADDR       0x21        
#define SSD1306_SET_PAGE_ADDR      0x22
#define SSD1306_DEAC_SCROLL        0x2E

// Register
#define SSD1306_CONTROL_REG 0x00
#define SSD1306_DATA_REG  0x40
unsigned char frame[1024];

// Sendet ein Kommando an das Display
void send_command(int fd, unsigned char command);


// Sendet Daten an das Display
void send_data(int fd, unsigned char data);

// Displayverbindung aufbauen und einstellen auf Standardwerte
int ssd1306_i2c_setup(int i2c_addr);

// Aktualisiert das Display immer nach draw_xx() ausführen
void update_display(int fd);

//I2C Adresse initialiseren
int ssd1306_desetup(int fd);

//Leert den Display
void clear_display(int fd);

//Schaltet Display ein
void display_on(int fd);

//Schaltet Display aus
void display_off(int fd);

// Schreibt ASCII Zeichen auf XY-Koordinate des Displays
void draw(int x, int y, unsigned char ascii);


// Lädt einen String von ASCII - Zeichen auf das Display
void draw_line(int x, int y, unsigned char ascii_str[16]);

//Funktion zur Darstellung des Standby- Modus
void display_standby(int s,int x,int y);

// Darstellung für : Finde die Mitte Modus
void display_find_mid(int s, float left_sens, float right_sens);


// Darstellung für : Für Abstanstsmessung
void display_distance(int address, float left_sens , float right_sens);


//Display wird vollständig initalisiert
int display_setup();

// Display wird deinitalisiert
void display_desetup(int address);
