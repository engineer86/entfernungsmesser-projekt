/*
*************************************************
Autoren: Philipp Horleander und Konrad Münch 
Projekt: Entferungsmesser für CSE 2020
Datum: April bis Juli 2020
*************************************************
Datei: button.h
Bauteilbibliothek für Button
 */


#ifndef BUTTON_H
#define BUTTON_H
#include <sys/types.h>
#include "gpio.h"
#include <stdbool.h>

#define PIN_BUTTON 5 // GPIO PIN

// Einfache Abfrage ob Button gedrückt wurde 
bool button_pressed();

// Entprellfunktion für Button
bool debounce_button(int delay);

#endif