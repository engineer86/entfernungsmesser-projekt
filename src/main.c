/*
*************************************************
Autoren: Philipp Horleander und Konrad Münch 
Projekt: Entferungsmesser für CSE 2020
Datum: April bis Juli 2020
*************************************************
Datei: main.c
Hauptfunktion

 */

#include "../lib/usonic.h"
#include "../lib/button.h"
#include "../lib/buzzer.h"
#include "../lib/display.h"

// LINKE SEITE (SD-KARTE)
#define PIN_LEFT_TRIG 17 //TRIGGER
#define PIN_LEFT_ECHO 05 //ECHO

//USB - SEITE (RECHTS)
#define PIN_RIGHT_TRIG  22 //TRIGGER
#define PIN_RIGHT_ECHO  27 // ECHO

#define SENSOR_DIST 9 //Länge des Raspberrys
#define TIMEOUT_MEASURE 10000 // Timeout für Messung

/* Variablen */
// globale statische Var. zum bestimmen des Betriebsmodi
static unsigned int current_mode =0;
 
 // Adresse des Displays
int display_address = 0;
// Display x und y Variablen für Display
static int x_frame[4] = {1,2,3,4}; 
static int y_frame[10] = {1,2,3,4,5,6,7,8,9,10};
int x_value = 1;
int y_value = 1;

float result_left = 0;
float result_right = 0;

/* Funktionen */

// Erfasst die Tastendrücke für Modiwechsel

int rec_mode()
{ 
	if(button_pressed() == true )
	{	
		current_mode = ++current_mode %3;
		printf("Modus: %d\n",current_mode);
		beep_change_mode();
		clear_display(display_address);

	}
	return current_mode;
}

// Funktionen zum Messen beider Sensoren
int measure_both(int timeout)
{
	int tmp_l = 0;
	int tmp_r = 0;
	int act_measurements = 0;
	
	result_left = 0;
	result_right = 0;
	
	for(int i = 0 ; i < 5 ; i++)
	{
		tmp_l = measure(PIN_LEFT_TRIG,  PIN_LEFT_ECHO,  timeout);
		tmp_r = measure(PIN_RIGHT_TRIG, PIN_RIGHT_ECHO, timeout);

		if ((tmp_l > -1) && (tmp_r > -1))
		{
			result_left += tmp_l;
			result_right += tmp_r;
			act_measurements++;
		}
		sleep(0.05);
	}

	result_left /= act_measurements;
	result_right /= act_measurements;

	return 0;
}

// Hauptfunktion 

int main()
{	
	// Setup des Displays
	display_address = display_setup();
	printf("Starten...\n");

	//Startsound
	beep_start_device();
	//Ausgabe des Modes auf der Console
	printf("Modus: %d\n",current_mode);

	//Setup der Sensoren *****************
	// SD-KARTEN - SEITE (LINKS)
  	export_pin(PIN_LEFT_TRIG);
  	export_pin(PIN_LEFT_ECHO);
  	pin_mode(PIN_LEFT_TRIG,OUT);
  	pin_mode(PIN_LEFT_ECHO,IN);

	//USB - SEITE (RECHTS)
  	export_pin(PIN_RIGHT_TRIG);
 	export_pin(PIN_RIGHT_ECHO);
  	pin_mode(PIN_RIGHT_TRIG,IN);
  	pin_mode(PIN_RIGHT_ECHO,OUT);
	//Setup der Sensoren *****************

	// Wiederholung des Hauptprogrammes
	while(1)
	{	
		//Standby wenn Modus 0 ist
		do
		{	
			display_standby(
				display_address,
				x_frame[x_value],	
				y_frame[y_value]
			);
			x_value++;
			y_value++;
			if(x_value > 3)
			{
				x_value = 1;
			}
			if(y_value > 10)
			{
				y_value = 1;
			}
					

		} while(rec_mode() == 0);

		//Enfernungsmessung wenn Modus 1 ist
		printf("JETZT MODUS 1\n");
	    do 
		{
			measure_both(TIMEOUT_MEASURE);
			printf("Links: %.2f cm\t Rechts %.2f cm\n", result_left, result_right);
			printf("Abstand: %.2f cm\n", result_left + result_right + SENSOR_DIST);
			display_distance(display_address, result_left, result_right);		
		
		} while(rec_mode() == 1);
		
		//Mittelpunktmessung wenn Modus 2 ist
		printf("JETZT MODUS 2\n"); 
		do
		{	
			measure_both(TIMEOUT_MEASURE);
			printf("Links: %.2f cm\t Rechts %.2f cm\n", result_left, result_right);
			printf("Differenz: %.2f cm\n", result_left - result_right);
			display_find_mid(display_address,result_left,result_right);	
			beep_middle(result_left ,result_right);

		} while(rec_mode() == 2);
	}
}