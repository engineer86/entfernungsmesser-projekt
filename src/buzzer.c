/*
*************************************************
Autoren: Philipp Horleander und Konrad Münch 
Projekt: Entferungsmesser für CSE 2020
Datum: April bis Juli 2020
*************************************************
Datei: buzzer.c
Bauteilbibliothek für Buzzer

 */

#include "../lib/buzzer.h"

// Funktionen des Buzzers

//Dauerhaftes Piepen des Buzzers
int beep_all_the_time_start()
{
	export_pin(BUZZER_PIN); 
	pin_mode(BUZZER_PIN,1);
	pin_write_value(BUZZER_PIN,1);
	return 0;
}

//Ton aus an Buzzer
int beep_all_the_time_stop()
{	
	pin_write_value(BUZZER_PIN,0);
	unexport_pin(BUZZER_PIN); 
	return 0;
}


// Tonsignal wenn Modus gewechselt wird
void beep_change_mode()
{
	export_pin(BUZZER_PIN);
	pin_mode(BUZZER_PIN, OUT);

	pin_write_value(BUZZER_PIN,1);
	usleep(DELAY_DURATION_CHANGE_MODE);
	pin_write_value(BUZZER_PIN,0);
	usleep(DELAY_DURATION_CHANGE_MODE);

	pin_write_value(BUZZER_PIN,1);
	usleep(DELAY_DURATION_CHANGE_MODE);
	pin_write_value(BUZZER_PIN,0);
	usleep(DELAY_DURATION_CHANGE_MODE);

	unexport_pin(BUZZER_PIN);
}


// Gibt Ton für Starten aus
void beep_start_device()
{
	export_pin(BUZZER_PIN); 
	pin_mode(BUZZER_PIN,1);

		for (int i = 0; i < 10; i++)
		{
			pin_write_value(BUZZER_PIN,1);
			usleep(DELAY_DURATION_START_SOUND);
			pin_write_value(BUZZER_PIN,0);
			usleep(DELAY_DURATION_START_SOUND);
		}
	unexport_pin(BUZZER_PIN);
}



// Funktion um Piepton auszugeben wenn Mittelpunkt gefunden wurde
void beep_middle(float left_sens,float right_sens)
{	
	
	unsigned int left = left_sens;
	unsigned int right = right_sens;
	
	int diff = abs(left - right);

	if(left == right)
	{
		beep_all_the_time_start();
	}
	else
	{
		beep_all_the_time_stop();
	}
}

