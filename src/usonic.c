/*
*************************************************
Autoren: Philipp Horleander und Konrad Münch 
Projekt: Entferungsmesser für CSE 2020
Datum: April bis Juli 2020
*************************************************
Datei: usonic.c
Bauteilbibliothek : Ultraschallsensorik

 */

#include "../lib/usonic.h"

// Ermittelt Referenzeit
uint64_t ref_time_micro = 0;

int init_ref_time()
{
	long	us;
	time_t	s;
 	struct timespec spec;

	clock_gettime(CLOCK_MONOTONIC, &spec);
	
	s = spec.tv_sec;
	us = round(spec.tv_nsec * 1e3);

	ref_time_micro = s * 1e6 + us;
}

// Funktions für Microsekunden
uint64_t microsec()
{	
	unsigned long	 us;
	time_t	 s;
 	struct timespec spec;

	clock_gettime(CLOCK_MONOTONIC , &spec);
	
	s = spec.tv_sec;
	us = spec.tv_nsec / 1.0e3; 

	return (uint64_t)(s * 1.0e-3) + (uint64_t)us; 
}

// Messfunktion für Ultraschallsensor
float measure(int p_trig, int p_echo, int timeout) 
{

	// Variablen für Messung
	int echo = 0, previousEcho = 0, lowHigh = 0, highLow= 0;
	long startTime = 0, stopTime = 0, diffTime = 0;
	float rangeCM = 0;

	//Trigger wird auf LOW gesetzt
	pin_write_value(p_trig, 0);
	usleep(10);
	
	// Trigger sendent für 10us
	pin_write_value(p_trig, 1);
	usleep(10);
	pin_write_value(p_trig, 0);
	
	startTime = microsec();

	int counter = 0;
	
	while((lowHigh == 0 || highLow == 0) ) 
	
	{ 	
		counter ++;
    	previousEcho = echo; 
		echo = pin_read_value(p_echo);

    	if(0 == lowHigh && 0 == previousEcho && 1 == echo) 
		{
    		lowHigh = 1;
    		startTime = microsec();
    	}
    	if(1 == lowHigh && 1 == previousEcho && 0 == echo) 
		{
    		highLow = 1;
    		stopTime = microsec();
    	}
		if (counter > timeout)
		{
			return -1;
		}
	}
	diffTime = stopTime - startTime;
	
	// Endberechnung
	rangeCM = diffTime / 58 ;
	
	return rangeCM;	
}