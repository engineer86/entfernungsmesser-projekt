/*
*************************************************
Autoren: Philipp Horleander und Konrad Münch 
Projekt: Entferungsmesser für CSE 2020
Datum: April bis Juli 2020
*************************************************
Datei: button.c
Bauteilbibliothek für Button
 */
#include "../lib/button.h"


// Einfache Abfrage ob Button gedrückt wurde 
bool button_pressed()
{
	export_pin(PIN_BUTTON);
	pin_mode(PIN_BUTTON,IN);

	if(pin_read_value(PIN_BUTTON) == 1 )
	{	
		unexport_pin(PIN_BUTTON);
		sleep(1);
		return true;
	}
	else
	{
		return false;
	}

}

// Entprellfunktion für Button
bool debounce_button(int delay)
{
	int counter = 0;
	export_pin(PIN_BUTTON);
	pin_mode(PIN_BUTTON,IN);

	while(counter < delay)
	{	
		if (pin_read_value(PIN_BUTTON) == 1 ) 
		{   
			counter++;
		}
		else
		{
			counter = 0;
		}
		usleep(1000);

	}
	unexport_pin(PIN_BUTTON);
	return true;

}

