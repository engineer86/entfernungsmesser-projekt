/*
*************************************************
Autoren: Philipp Horleander und Konrad Münch 
Projekt: Entferungsmesser für CSE 2020
Datum: April bis Juli 2020
*************************************************
Datei: i2c.c
I2C-Bibliothek
*/

#include "../lib/i2c.h"

// Zugriff auf den SM-Bus
int i2c_smbus_access (int fd, char rw, uint8_t command, int size, union i2c_smbus_data *data)
{
  	struct i2c_smbus_ioctl_data message ;

  	message.read_write = rw;
  	message.command    = command;
  	message.size       = size;
  	message.data       = data;
  	return ioctl (fd, I2C_SMBUS, &message);
}

//Setup des I2C Slaves
int i2c_setup(int i2c_slave)
{

	int file = open("/dev/i2c-1", O_RDWR);
	if (file < 0)
	{
		perror("open i2c-1");
		exit(1);
	}

	if (ioctl(file, I2C_SLAVE, i2c_slave) < 0)
	{
		perror("cant access slave");
		exit(1);
	}
	return file;
}

// Schreiben in den Bus
int i2c_write(int fd, int data)
{
	return i2c_smbus_access(fd, I2C_SMBUS_WRITE, data, I2C_SMBUS_BYTE, NULL);
}

// Schreiben in das Register
int i2c_write_reg_8 (int fd, int reg, int data)
{
	union i2c_smbus_data message;

  	message.byte = data;
  	return i2c_smbus_access (fd, I2C_SMBUS_WRITE, reg, I2C_SMBUS_BYTE_DATA, &message);
}

//Lesen des SM-Bus Registers
int i2c_read_reg_8 (int fd, int reg)
{
	union i2c_smbus_data message;

  	if (i2c_smbus_access (fd, I2C_SMBUS_READ, reg, I2C_SMBUS_BYTE_DATA, &message))
    	return -1;
  	else
    	return message.byte & 0xFF;
}