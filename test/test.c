/*
*************************************************
Autoren: Philipp Horleander und Konrad Münch 
Projekt: Entferungsmesser für CSE 2020
Datum: April bis Juli 2020
*************************************************
Datei: test.c
Testprogramm für Ultraschallsensoren 
mit eigener Lib, ohne WiringPi

 */


#include "../lib/usonic.h"


float right = 0;
float left = 0;
bool flag = false;
int main()
{
// SD-KARTEN - SEITE (LINKS)
  export_pin(17);
  export_pin(05);
  pin_mode(17,1);
  pin_mode(05,0);

//USB - SEITE (RECHTS)
  export_pin(22);
  export_pin(27);
  pin_mode(22,1);
  pin_mode(27,0);


  while(left < 50.00 && right < 50.00)
  {
  
  left= 0;
  left =	measure(17,05,10000);
  right =0;
  right =  measure(22,27,10000);

  sleep(1);
  system("clear");
  printf(" Rechts: %0.2lf Links: %0.2lf \n",right,left);
  
 
  
  }
  unexport_pin(17);
  unexport_pin(05);
  unexport_pin(22);
  unexport_pin(27);
  
}

