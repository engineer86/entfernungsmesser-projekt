/*
*************************************************
Autoren: Philipp Horleander und Konrad Münch 
Projekt: Entferungsmesser für CSE 2020
Datum: April bis Juli 2020
*************************************************
Datei: test_usonic.c
Testprogramm für Ultraschallsensoren
mit WiringPi (Nur als Referenztest)
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <wiringPi.h>


#define nown

#ifdef own
	#include <stdbool.h>
#endif

// SD-KARTEN - SEITE (LINKS)
#define PIN_LEFT_TRIG 17 //TRIGGER
#define PIN_LEFT_ECHO 05 //ECHO

//USB - SEITE (RECHTS)
#define PIN_RIGHT_TRIG  22 //TRIGGER
#define PIN_RIGHT_ECHO  27 // ECHO

long getMicrotime(){
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);
  
	return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
}

float measure(int i_trig, int i_echo)
{
	int echo = 0, previousEcho = 0, lowHigh = 0, highLow = 0;
  	long startTime = 0, stopTime = 0, difference = 0;
  	float rangeCm = 0;
  	
	wiringPiSetup(); // U
  
 	pinMode(i_trig, OUTPUT);
  	digitalWrite(i_trig, LOW); //

  	pinMode(i_echo, INPUT); // 

  	digitalWrite(i_trig, HIGH);
  	usleep(10);
  	digitalWrite(i_trig, LOW);
  		
#ifdef own
	bool flag = false;
	int timeout = 0;

	while(1)
	{	
		echo = digitalRead(i_echo);  

		if (flag == false && echo == 1)
		{
			flag == true;
		} else if (flag == true && echo == 0)
		{
			stopTime = getMicrotime();
			break;
		}
		if (timeout++ > 1000 * 1000)
		{
			return -1;
		}
	}
#endif

#ifdef nown
  	while(0 == lowHigh || highLow == 0) {
    	previousEcho = echo;
    	echo = digitalRead(i_echo);
    	if(0 == lowHigh && 0 == previousEcho && 1 == echo) {
    		lowHigh = 1;
    		startTime = getMicrotime();
    	}
    	if(1 == lowHigh && 1 == previousEcho && 0 == echo) {
    		highLow = 1;
    		stopTime = getMicrotime();
    	}
	}	
#endif

  	difference = stopTime - startTime;
  	rangeCm = difference / 58;

	return rangeCm;
}

int main(void)
{
	while (1) {
		float left10 = 0, right10 = 0;
		for(int i = 0; i < 10; i++)
		{
			delay(100);
			left10 	 += measure(PIN_LEFT_TRIG, PIN_LEFT_ECHO);
			right10  += measure(PIN_RIGHT_TRIG, PIN_RIGHT_ECHO);
		}
		
		system("clear");
		printf("Right: %.2f cm\n", right10 / 10);
		printf("Left: %.2f cm\n", left10 / 10);
	}
}